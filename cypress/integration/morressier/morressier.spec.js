describe("Sign Up feature", () => {
  before(() => {
    cy.visit('/');
  });

  beforeEach(function () {
    cy.fixture("morressier/user").then(function (user) {
      this.user = user;
    });
  });

  it("Check if the Sign Up button is available", () => {
    cy.findByRole("button", { name: /Signup/i })
      .should("exist")
      .and("be.visible");
  });

  it("Click on Sign Up and check the pop-up title", () => {
    cy.findByRole("button", { name: /Signup/i }).click();
    cy.get("h2").should("have.text", "Sign in or create your account");
  });

  it("Check if ORCID alternative log in is displayed", () => {
    cy.findByRole("button", { name: /ORCID ID/i }    )
      .should("exist")
      .and("be.visible");
  });

  it("Check if ACS alternative log in is displayed", () => {
    cy.findByRole("button", { name: /ACS ID/i })
      .should("exist")
      .and("be.visible");
  });

  it("Check if APS alternative log in is displayed", () => {
    cy.findByRole("button", { name: /APS ID/i })
      .should("exist")
      .and("be.visible");
  });

  it("Check if the email placeholder is displayed and insert an email", function () {
    cy.get(`[data-test-id="FORWARDER_EMAIL"]`)
      .clear()
      .type(this.user.email)
      .should("be.visible");
  });

  it("Check if the Next button is displayed", () => {
    cy.findByRole("button", { name: /Next/i })
      .should("exist")
      .and("be.visible");
  });

  it("Click on Next button and check the error message", () => {
    cy.findByRole("button", { name: /Next/i }).click();
    cy.get(`[data-test-id="GENERAL_FORM_FEEDBACK"]`)
      .should("have.text", "We don't recognize this email address. Try again?")
      .and("be.visible");
  });

  it("Check if Request an invite hyperlink is displayed", () => {
    cy.get(`[data-test-id="NEW_TO_MORRESSIER"]`).should("be.visible");
  });

  it("Click on Request an invite hyperlink and check the title", () => {
    cy.get(`[data-test-id="NEW_TO_MORRESSIER"]`).children().click();
    cy.get("h2").should("have.text", "Request an invite");
  });

  it("Check if the name placeholder is displayed and add a name", function () {
    cy.get(`[name="full_name"]`)
      .clear()
      .type(this.user.name)
      .should("be.visible");
  });

  it("Check if the company placeholder is displayed and add a company name", function () {
    cy.get(`[data-test-id="WAITLIST_ORGANIZATION"]`)
      .clear()
      .type(this.user.company)
      .should("be.visible");
  });

  it("Check if the department placeholder is displayed and add a department name", function () {
    cy.get(`[name="department"]`)
      .clear()
      .type(this.user.department)
      .should("be.visible");
  });

  it("Check if the Request invite button is displayed", () => {
    cy.findByRole("button", { name: /Request invite/i })
      .should("exist")
      .and("be.visible");
  });

  //I've sent a request, sorry(removed that test)
});

describe("Log In feature", () => {
  before(() => {
    cy.visit("https://www.morressier.com/event/5e733c5acde2b641284a7e27");
  });

  beforeEach(function () {
    cy.fixture("morressier/user").then(function (user) {
      this.user = user;
    });
  });

  it("Check if the Log In button is available", () => {
    cy.findByText("Login").should("exist").and("be.visible");
  });

  it("Click on Log in button and check the pop-up title", () => {
    cy.findByText("Login").click();
    cy.get("h2").should("have.text", "Sign in or create your account");
  });

  it("Check if ORCID alternative log in is displayed", () => {
    cy.findByRole("button", { name: /ORCID ID/i })
      .should("exist")
      .and("be.visible");
  });

  it("Check if ACS alternative log in is displayed", () => {
    cy.findByRole("button", { name: /ACS ID/i })
      .should("exist")
      .and("be.visible");
  });
  it("Check if APS alternative log in is displayed", () => {
    cy.findByRole("button", { name: /APS ID/i })
      .should("exist")
      .and("be.visible");
  });

  it("Check if the email placeholder is displayed and insert an email", function () {
    cy.get(`[data-test-id="SIGNIN_EMAIL"]`)
      .clear()
      .type(this.user.email)
      .should("be.visible");
  });

  it("Check if the password placeholder is displayed and insert a password", function () {
    cy.get(`[data-test-id="SIGNIN_PASSWORD"]`)
      .clear()
      .type(this.user.password)
      .should("be.visible");
  });

  it("Check if the Log In button is displayed", () => {
    cy.findByRole("button", { name: /Log in/i })
      .should("exist")
      .and("be.visible");
  });

  it("Click on Log In button and check the error message", () => {
    cy.findByRole("button", { name: /Log in/i }).click();
    cy.get(`[data-test-id="GENERAL_FORM_FEEDBACK"]`)
      .should(
        "have.text",
        "Something went wrong, please check your credentials and try again"
      )
      .and("be.visible");
  });

  it("Check if Forgot password hyperlink is displayed", () => {
    cy.get(`[data-test-id="SIGNIN_FORGOT_PASSWORD_LINK"]`)
      .should("be.visible")
      .and("have.text", "Forgot password?");
  });

  it("Click on Forgot password hyperlink and check the title", () => {
    cy.get(`[data-test-id="SIGNIN_FORGOT_PASSWORD_LINK"]`).click();
    cy.get("h2").should("have.text", "Password recovery");
  });

  it("Check if the email placeholder is displayed and insert an email", function () {
    cy.get(`[data-test-id="REQUEST_PASSWORD_EMAIL_INPUT"]`)
      .clear()
      .type(this.user.email)
      .should("be.visible");
  });

  it("Check if the Get recovery Link button is displayed", () => {
    cy.findByRole("button", { name: /Get recovery link/i })
      .should("exist")
      .and("be.visible");
  });

  it("Click on Get recovery Link button and check the error message", () => {
    cy.findByRole("button", { name: /Get recovery link/i }).click();
    cy.get(`[data-test-id="MODAL_ERROR_MESSAGE"]`)
      .should("have.text", "Something went wrong, please try again!")
      .and("be.visible");
  });
});

describe("Filter submissions", () => {
  before(() => {
    cy.visit("https://www.morressier.com/event/5e733c5acde2b641284a7e27");
  });

  it("Check if the Add filter button is available", () => {
    cy.findByRole("button", { name: /Add filter/i })
      .scrollIntoView()
      .should("exist")
      .and("be.visible");
  });
  it("Check if the Add filter button icon is displayed", () => {
    cy.findByRole("button", { name: /Add filter/i })
      .find("svg")
      .scrollIntoView()
      .should("be.visible");
  });

  it("Click on Filter button and check if the pop-up title is displayed", () => {
    cy.findByRole("button", { name: /Add filter/i }).click();
    cy.findByText("Presentation filters").should("be.visible");
  });

  it("Check if the session section and it's search placeholder are displayed, then check the first two options", () => {
    cy.filterSection({
      sectionName: "session",
      searchName: "Search for session",
    });

    cy.findByRole("button", {
      name: /Undergraduate Research Posters/i,
    }).click();

    cy.findByRole("button", { name: /Sci-Mix/i }).click();
  });

  it("Check if the symposia section and it's search placeholder are displayed", () => {
    cy.filterSection({
      sectionName: "symposia",
      searchName: "Search for symposia",
    });
  });

  it("Check if the division section and it's search placeholder are displayed", () => {
    cy.filterSection({
      sectionName: "division",
      searchName: "Search for division",
    });
  });

  it("Check if the organizations section and it's search placeholder are displayed", () => {
    cy.filterSection({
      sectionName: "Organizations",
      searchName: "Search for organizations",
    });
  });

  it("Check if the author names section and it's search placeholder are displayed", () => {
    cy.filterSection({
      sectionName: "Author names",
      searchName: "Search for author names",
    });
  });

  it("Check if the keywords section and it's search placeholder are displayed", () => {
    cy.filterSection({
      sectionName: "Keywords",
      searchName: "Search for keywords",
    });
  });

  it("Check if the Close button is displayed properly", () => {
    cy.findByRole("button", { name: /Close/i })
      .scrollIntoView()
      .should("be.visible");
  });

  it("Close filters module and check if the filters counter is displaying the correct number", () => {
    cy.findByRole("button", { name: /Close/i }).click();
    cy.findByRole("button", { name: /Filters/i })
      .should("be.visible")
      .and("have.text", "Filters (2)");
  });

  it("Remove all filters and check again the Filter counter", () => {
    cy.findByRole("button", { name: /Filters/i }).click();
    cy.findByText("Clear all filters").should("be.visible").click();
    cy.findByRole("button", { name: /Close/i }).scrollIntoView().click();
    cy.findByRole("button", { name: /Add filter/i })
      .should("be.visible")
      .and("have.text", "Add filter");
  });
});
