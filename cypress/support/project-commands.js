Cypress.Commands.add("filterSection", (params) => {
  if (params.sectionName === "Keywords") {
    cy.findByText(`${params.sectionName}`)
      .scrollIntoView()
      .should("be.visible");
  } else {
    cy.findByText(`${params.sectionName}`)
      .scrollIntoView()
      .should("be.visible")
      .click();
  }
  cy.findByPlaceholderText(`${params.searchName}`)
    .scrollIntoView()
    .should("exist")
    .and("be.visible");
});
